# 1 - ESTRUCTURAR CONFIGURACIONES - TERRAFORM

Crear estructura y separa los recursos en sus archivos .tf correspondientes :

- provider.tf
- main.tf
- variables.tf
- terraform.tfvars



# 2 - CREAR NUEVOS RECURSOS - TERRAFORM

Recursos GPC: https://registry.terraform.io/providers/hashicorp/google/latest/docs


Agregue la siguiente configuración para un recurso de  "Google compute instance" al ```main.tf```.
```
resource "google_compute_instance" "vm_instance" {
  name         = "terraform-instance"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
    }
  }
}
```

# 3 - DEFINIR OUTPUT

Cree un archivo llamado outputs.tf con el siguiente contenido para definir una salida de los parámetros de configuración:

```
output "ip" {
  value = google_compute_instance.vm_instance.network_interface.0.network_ip
}
```

Directorio de referencia: ( [**p1**](./example/p1) ) 

# 4 - MODULOS

A medida que maduremos nuestra infraestructura, las estructuras serán cada vez mas complejas siendo muy frecuente encontrar problemas como:
- Comprender y navegar por los archivos de configuración será cada vez más difícil.
- Actualizar la configuración será más arriesgado
- Habrá una cantidad cada vez mayor de duplicación de bloques de configuración similares
- Es posible que desee compartir partes de su configuración entre proyectos y equipos.

¿Para que son los modulos?
- Organice la configuración
- Encapsular la configuración
- Reutilizar configuraciones
- Reducir errores en la reutilización.

¿Qué es un módulo Terraform?
Un módulo de Terraform es un conjunto de archivos de configuración de Terraform en un solo directorio. Incluso una configuración simple que consta de un único directorio con uno o más archivos .tf es un módulo.

```
.
├── LICENSE
├── README.md
├── main.tf
├── variables.tf
├── outputs.tf

```

## Crear directorio de modulos

Creamos el directorio "modules" y asignamos un sub directorio aplicando las buenas practicas en el nombre de modulos "terraform-[PROVIDER]-[NAME]".

```
.
├── modules
    ├── terraform-[PROVIDER]-[NAME]
        ├── main.tf
        ├── variables.tf
        ├── outputs.tf
├── main.tf
├── variables.tf
├── provider.tf
├── terraform.tfvar

```
Directorio de referencia: ( [**p2**](./example/p2) ) 


# 5 - MODULARIZAR: VPC

## Crear estructura del modulo
Creamos el directorio "modules" y asignamos un sub directorio aplicando las buenas practicas en el nombre de modulos "terraform-[PROVIDER]-[NAME]", y mover las configuraciones de la vpc hacia el modulo.

Directorio de referencia: ( [**vpc-module**](./example/p2/modules/terraform-gcp-vpc) ) 

```
.
├── modules                   <- <create>
    ├── terraform-gcp-vpc     <- <create>
        ├── main.tf           <- <create>
        ├── variables.tf      <- <create>
        ├── outputs.tf        <- <create>
├── main.tf
├── variables.tf
├── provider.tf
├── terraform.tfvar

```

## Modificar el main raiz

Referenciamos el modulo de vpc y asignamos las variables correspondientes

```
.
├── modules                   
    ├── terraform-gcp-vpc     
        ├── main.tf          
        ├── variables.tf     
        ├── outputs.tf        
├── main.tf                   <- <Modify>
├── variables.tf
├── provider.tf
├── terraform.tfvar

```

```
module "vpc" {
  source        = "modules/terraform-gcp-vpc"
  network_name  = "my_network"
}

```