## Resources

module "vpc" {
  source        = "./modules/terraform-gcp-vpc"
  network_name  = "my_network"
}

module "instance" {
  source          = "./modules/terraform-gcp-compute-engine"
  name            = "terraform-instance"
  machine_type    = "f1-micro"
  image           = "debian-cloud/debian-9"
  network         = module.vpc.output_network_name
}